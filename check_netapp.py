#!/home/anaconda/bin/python

# F.Martinelli - 18-02-2014

"""
# /opt/nagios/check_netapp02.py  --netapp01 rmsgi01 --netapp02 rmsgi02 
ONLINESTATUS OK - NetApp Status ( rmsgi01, rmsgi02 ) is Optimal. | BatteriesNotOK=0;@1:100 DrawersNotOK=0;@1:100 ESMcardsNotOK=0;@1:100 PowerSuppliesNotOK=0;@1:100 SFPNotOK=0;@1:100
TempsNotOK=0;@1:100 VolumesNotOK=0;@1:100
"""

import nagiosplugin  # http://pythonhosted.org/nagiosplugin
import logging       # http://docs.python.org/2/library/logging.html
import argparse

_log = logging.getLogger('nagiosplugin')


class NetAppStatus(nagiosplugin.Resource):

   def __init__(self): 
       # we're going to accumulate the not optimal lines splitted by category
       self.volumesNotOK       = []
       self.ESMcardsNotOK      = []
       self.PowerSuppliesNotOK = []
       self.DrawersNotOK       = []
       self.TempsNotOK         = []
       self.BatteriesNotOK     = []
       self.SFPNotOK           = []
     
       # A list of strings ['Performing syntax check...', '', 'Syntax check complete.', '', 'Executing script...', ...
       self.mylines            = [] 
   
       

   def probe(self):
       #assert type(self.mylines) is []
       #assert self.mylines       <> []
 
       lineNumber=0
       # Searching for NOT optimal states in self.mylines ##
       for line in self.mylines:

         lineNumber += 1

         if        "status:" in line:
            lineNumerStr = '{:>5}'.format( str(lineNumber) )
            _log.debug(
                 'line #' + lineNumerStr + ' : ' + line)        
                                     
         if "Volume status:" in line             and "Optimal" not in line:
            _log.info('Found not Optimal Volume status: '             + line)
            self.volumesNotOK.append(line)

         if "ESM card status:" in line           and "Optimal" not in line:
            _log.info('Found not Optimal ESM card status: '           + line)
            self.ESMcardsNotOK.append(line)

         if "Power supply status:" in line       and "Optimal" not in line:
            _log.info('Found not Optimal Power supply status: '       + line)        
            self.PowerSuppliesNotOK.append(line)

         if "Drawer status:" in line             and "Optimal" not in line:
            _log.info('Found not Optimal Drawer status: '             + line) 
            self.DrawersNotOK.append(line) 

         if "Temperature sensor status:" in line and "Optimal" not in line:
            _log.info('Found not Optimal Temperature sensor status: ' + line)
            self.TempsNotOK.append(line)

         if "Battery status:" in line            and "Optimal" not in line:
            _log.info('Found not Optimal Battery status: '            + line)
            self.BatteriesNotOK.append(line)
         
         if "SFP status:" in line                and "Optimal" not in line:
            _log.info('Found not Optimal SFP status: '                + line)
            self.SFPNotOK.append(line)  
       ########################################

      
           # nagiosplugin.Metric( name,          value, uom=None,           min=None, max=None, context=None, contextobj=None, resource=None)
       yield nagiosplugin.Metric('VolumesNotOK' ,     len(self.volumesNotOK),       context='scalar')
       yield nagiosplugin.Metric('ESMcardsNotOK',     len(self.ESMcardsNotOK),      context='scalar')
       yield nagiosplugin.Metric('PowerSuppliesNotOK',len(self.PowerSuppliesNotOK), context='scalar')
       yield nagiosplugin.Metric('DrawersNotOK',      len(self.DrawersNotOK),       context='scalar')
       yield nagiosplugin.Metric('TempsNotOK',        len(self.TempsNotOK),         context='scalar')
       yield nagiosplugin.Metric('BatteriesNotOK',    len(self.BatteriesNotOK),     context='scalar')
       yield nagiosplugin.Metric('SFPNotOK',          len(self.SFPNotOK),           context='scalar')

# nagiosplugin.context.ScalarContext(name, 
#                                    warning=None, 
#                                    critical=None, 
#                                    fmt_metric=None, 
#                                    result_cls=<class 'nagiosplugin.result.ScalarResult'>)


class OnLineStatus(NetAppStatus):

   def __init__(self,netapp01,netapp02,SMcli):
       NetAppStatus.__init__(self)
       # We check if we can really reach those hosts
       # python ping | wget "https://bitbucket.org/delroth/python-ping/downloads/python-ping-0.2.tar.gz" --no-check-certificate 
       import ping, socket
       assert None <> ping.quiet_ping(netapp01, count=1, psize=64)[1]
       self.netapp01                = netapp01
       assert None <> ping.quiet_ping(netapp02, count=1, psize=64)[1]
       self.netapp02                = netapp02 
      
       import os,stat 
       assert os.stat(SMcli)[stat.ST_SIZE] > 0
       assert os.access(SMcli, os.X_OK)

       import shlex, subprocess
       command_line = SMcli + ' ' + self.netapp01 + ' ' + self.netapp02 + ' -c "show storageArray profile;"' 
       _log.warning( 'running : ' + command_line )
       args         = shlex.split(command_line)     # e.g. args = ['/usr/bin/SMcli', 'netapp01', 'netapp02', '-c', 'show storageArray profile;']
       mystdoutput  = subprocess.check_output(args) # A very long string "Performing syntax check... ..."
   
       self.mylines = mystdoutput.splitlines()      # A list of strings ['Performing syntax check...', '', 'Syntax check complete.', '', 'Executing script...', ...
     

class OffLineStatus(NetAppStatus):

   def __init__(self,SMclioutput):
       NetAppStatus.__init__(self)
       import os,stat
       assert os.stat(SMclioutput)[stat.ST_SIZE] > 0
       self.mylines = open(SMclioutput).readlines() 


##### Summaries ##################################

class OnLineSummary(nagiosplugin.Summary):

    def __init__(self,netapp01,netapp02):
       import ping, socket
       assert None <> ping.quiet_ping(netapp01, count=1, psize=64)[1]
       self.netapp01                = netapp01
       assert None <> ping.quiet_ping(netapp02, count=1, psize=64)[1]
       self.netapp02                = netapp02

    # results = class nagiosplugin.result.Results(*results)
    # http://pythonhosted.org/nagiosplugin/api/intermediate.html#nagiosplugin.result.Results
    # where
    # result  = class nagiosplugin.result.Result
    # http://pythonhosted.org/nagiosplugin/api/intermediate.html#nagiosplugin.result.Result
    def ok(self, results):
       return 'NetApp Status ( '+ self.netapp01 + ', ' + self.netapp02 + ' ) is Optimal.' 

    def problem(self, results):
       return 'NetApp Status ( '+ self.netapp01 + ', ' + self.netapp02 + ' ) is NOT Optimal.'


class OffLineSummary(nagiosplugin.Summary):

    def __init__(self,SMclioutput):
       self.SMclioutput = SMclioutput 

    # results = class nagiosplugin.result.Results(*results)
    # http://pythonhosted.org/nagiosplugin/api/intermediate.html#nagiosplugin.result.Results
    # where
    # result  = class nagiosplugin.result.Result
    # http://pythonhosted.org/nagiosplugin/api/intermediate.html#nagiosplugin.result.Result
    def ok(self, results):
       return 'NetApp Status is Optimal according to ' + str(self.SMclioutput) 

    def problem(self, results):
       return 'NetApp Status is NOT Optimal according to ' + str(self.SMclioutput)

################################################

@nagiosplugin.guarded
def main():

    argp = argparse.ArgumentParser(
        description=
         "Nagios E5400/E5500/DS3700 and similar systems check." +
         "Either you provide the --SMclioutput option, or the --netapp01,--netapp02,--SMcli options."
       ,
        usage='%(prog)s [options]'
       )

    argp.add_argument("--netapp01",    default="", help='1st IP / HOSTNAME to connect to the E5400/E5500/DS3700')
    argp.add_argument("--netapp02",    default="", help='2nd IP / HOSTNAME to connect to the E5400/E5500/DS3700')
    argp.add_argument("--SMcli",       default="/usr/bin/SMcli", help='NetApp SMcli tool path. Default is : /usr/bin/SMcli')

    argp.add_argument("--SMclioutput", default="",               help='File where you saved a SMcli output ( e.g. by cron )')
   

    argp.add_argument('-v', '--verbose', action='count', default=0, help='Output verbosity (use up to 3 times)')

    #args = argp.parse_args()
    args, unknown = argp.parse_known_args()
   
    printusage = False
    # args.SMcli is always True because of its default value, so we don't include it in the next boolean expression
    if unknown                                                         : printusage = True
    if args.SMclioutput and ( args.netapp01 or args.netapp02 )         : printusage = True
    if not args.SMclioutput and not ( args.netapp01 or args.netapp02 ) : printusage = True
     
    if printusage:
        print "Either wrong or too many options, check the prog usage.\n" 
        argp.print_help()
        import sys
        sys.exit(-1)

    if not args.SMclioutput :
      check = nagiosplugin.Check(
                OnLineStatus(
                     args.netapp01,
                     args.netapp02,
                     args.SMcli
                ),
                nagiosplugin.ScalarContext('scalar', warning="@1:100" ),
                OnLineSummary(
                     args.netapp01,
                     args.netapp02
                )
              )
    else : 
      check = nagiosplugin.Check(
                OffLineStatus(
                     args.SMclioutput
                ),
                nagiosplugin.ScalarContext('scalar', warning="@1:100" ),
                OffLineSummary(
                     args.SMclioutput
                )
              )
    # Invoking nagiosplugin.Resource.probe()          
    check.main(
               verbose=args.verbose,
               timeout=120
              )

if __name__ == '__main__':
    main()





